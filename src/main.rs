mod parser;

use std::fs;

fn main() {
    let input = fs::read_to_string("source_file.ol").expect("File cannot be opened.");

    parser::parse_tree_printer::print_parse_tree(&input);
}
