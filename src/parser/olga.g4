grammar olga;

compilationUnit : externalDeclaration+;

//DECLARATIONS

externalDeclaration
    : functionDefinition
    | declaration ';'
    ;

functionDefinition
    : declarationSpecifiers declarator '(' parameterDeclarationList? ')' '{' compoundStatement* '}'
    ;

parameterDeclarationList: declaration (',' declaration)*;

declaration: declarationSpecifiers declarator? ('=' initializer)?;

declarationSpecifiers: declarationSpecifier+;

declarationSpecifier
    : typeSpecifier
    | typeQualifier
    ;

typeSpecifier: Int | Char | Float | Void | structSpecifier;

typeQualifier: 'const' | 'volatile';

structSpecifier
    : Struct Identifier '{' structDeclarationList? '}'
    | Struct Identifier
    ;

structDeclarationList: structDeclaration+;

structDeclaration: declarationSpecifiers declarator ';'; //TODO: throw error if no semicolon

initializer
    : (Literal | Identifier)
    | expression
    | '{' initializer? (',' initializer)* '}'
    ;

declarator: pointer? directDeclarator;

pointer: '*';

directDeclarator
    : Identifier
    | Identifier '[' Identifier? ']'
    | Identifier '[' Literal? ']'       //TODO: check it during compilation and allow ONLY numbers as indexes
    ;

//STATEMENTS

compoundStatement
    : jumpStatement
    | declaration ';'
    | expression ';'
    | ifStatement
    | whileStatement
    | functionCall ';'
    ;

jumpStatement
    : Return (Literal | Identifier) ';'
    | Break ';'
    | Continue ';'
    ;

ifStatement : 'if' '(' expression ')' '{' compoundStatement* '}' ('else' compoundStatement*)?;

whileStatement : 'while' '(' expression ')' '{' compoundStatement* '}';

functionCall: Identifier '(' expression? (',' expression)* ')';

//EXPRESSIONS

expression
    : '(' expression ')'
    | assignmentExpression
    | unaryExpression
    | (Identifier | Literal)
    | functionCall
    | expression (relationalOperator expression)+
    | expression (boolOperator expression)+
    | expression (binaryOperator expression)+
    ;

assignmentExpression
    : unaryExpression assignmentOperator expression
    ;

assignmentOperator: '=' | '+=' | '-=' | '*=' | '/=';

unaryExpression
    : ('++' | '--')*
    ( postfixExpression
    | unaryOperator? '(' expression ')'
    | unaryOperator? Identifier
    );


postfixExpression
    :
    (
        '(' expression ')' | Identifier | Literal
    )
    (
        ('.' | '->') (Identifier | functionCall) //FIXME, NIE CZYTA FUNC_CALL
        | ('++' | '--')
    )
    ;

unaryOperator: '&' | '*' | '+' | '-' | '~' | '!';

boolOperator: '||' | '&&';

relationalOperator: '<' | '>' | '<=' | '>=' | '!='| '==';

binaryOperator: '+' | '-' | '*' | '/' | '%';


//LEXER

//keywords
Int : 'int';
Char : 'char';
Float : 'float';
Void : 'void';
Struct: 'struct';
Break: 'break';
Continue: 'continue';
Return : 'return';

//spearators
LeftParen : '(';
RightParen : ')';
LeftBracket : '[';
RightBracket : ']';
LeftBrace : '{';
RightBrace : '}';

Semicolon : ';';

//identifiers
Identifier : Letter(Letter | Number)*;

//literals
Literal
    : '"' (Letter | Number)+ '"'
    | 'true'
    | 'false'
    | Number+
    | Number+ '.' Number+
    ;



fragment
Letter: [a-zA-Z_];

fragment
Number: [0-9];

//other
WS : [ \t\r\n]+ -> skip ;

LINE_COMMENT : '//' ~[\r\n]* -> skip ;