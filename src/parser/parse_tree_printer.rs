use antlr_rust::common_token_stream::CommonTokenStream;
use antlr_rust::InputStream;
use antlr_rust::tree::ParseTree;

use super::grammar::olgalexer::*;
use super::grammar::olgaparser::*;

pub fn print_parse_tree(input: &str) {
    let lexer = olgaLexer::new(InputStream::new(input.into()));
    let source = CommonTokenStream::new(lexer);
    let mut parser = olgaParser::new(source);


    let parse_tree_root = parser.compilationUnit().expect("Parse tree root node");


    println!("{}", parse_tree_root.to_string_tree(&*parser));
}