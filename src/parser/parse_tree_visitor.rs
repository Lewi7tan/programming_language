#![allow(unused_imports)]
#![allow(unused_variables)]
#![allow(non_snake_case)]

use antlr_rust::common_token_stream::CommonTokenStream;
use antlr_rust::input_stream::InputStream;
use antlr_rust::parser_rule_context::{ParserRuleContext, RuleContextExt};
use antlr_rust::tree::{ParseTree, ParseTreeVisitor, Tree, Visitable};

use crate::parser::ast::*;
use crate::parser::grammar::olgavisitor::olgaVisitor;
use super::grammar::olgalexer::*;
use super::grammar::olgaparser::*;
use super::grammar::olgalistener::*;

//         let start = _ctx.start().get_line();

pub struct Visitor {
    pub root_node: Vec<AST>,
    declarations: Vec<Declaration>,
    statements: Vec<Statement>,
    expressions: Vec<Expression>,
    declaration_specifiers: Vec<DeclarationSpecifier>,
}

impl Visitor {
    pub fn new() -> Self {
        Visitor {
            root_node: vec![],
            declarations: vec![],
            statements: vec![],
            expressions: vec![],
            declaration_specifiers: vec![],
        }
    }
}

impl<'node> ParseTreeVisitor<'node, olgaParserContextType> for Visitor {}

impl<'node> olgaVisitor<'node> for Visitor {
    fn visit_compilationUnit(&mut self, ctx: &CompilationUnitContext<'node>) {
        ctx.accept_children(self);
        println!("{:?}", self.root_node);
        println!("\nCompilation Unit: {}", ctx.get_text());
    }

    fn visit_functionDefinition(&mut self, ctx: &FunctionDefinitionContext<'node>) {
        let name = ctx.declarator().unwrap().get_text();


        let mut specifiers: Vec<DeclarationSpecifier> = Vec::new();
        ctx.declarationSpecifiers().unwrap().accept_children(self);
        for specifier in 0..ctx.declarationSpecifiers().unwrap().get_child_count() { //FIXME: WHY 0?
            specifiers.push(self.declaration_specifiers.pop().unwrap());
        }
        specifiers.reverse();


        let mut parameters: Vec<Declaration> = Vec::new();
        ctx.parameterDeclarationList().unwrap().accept_children(self);
        for parameter in 1..ctx.parameterDeclarationList().unwrap().get_child_count() {
            parameters.push(self.declarations.pop().unwrap());
        }
        parameters.reverse();


        let mut compound_statements: Vec<Statement> = Vec::new();
        for statement in ctx.compoundStatement_all().iter() {
            if statement.declaration().is_some() {
                statement.accept(self);
                compound_statements.push(Statement::Declaration(Box::from(self.declarations.pop().unwrap())));
            }
        }


        let node = ExternalDeclaration::FunctionDefinition(Box::from(FunctionDefinition {
            name,
            parameters,
            body: compound_statements,
            ret: specifiers,
        }));

        self.root_node.push(node);
    }

    fn visit_declaration(&mut self, ctx: &DeclarationContext<'node>) {
        let name = ctx.declarator().unwrap().get_text();

        if ctx.declarator().unwrap().pointer().is_some() {
            let pointer = Expression::UnaryExpression();
        }


        let mut specifiers: Vec<DeclarationSpecifier> = Vec::new();
        ctx.declarationSpecifiers().unwrap().accept_children(self);
        for specifier in 0..ctx.declarationSpecifiers().unwrap().get_child_count() {
            specifiers.push(self.declaration_specifiers.pop().unwrap());
        }
        specifiers.reverse();


        let decl = Declaration {
            name,
            declaration_specifiers: specifiers,
            initializer: None,
        };

        self.declarations.push(decl);
    }

    fn visit_typeSpecifier(&mut self, ctx: &TypeSpecifierContext<'node>) {
        //FIXME: should be match statement
        if ctx.Int().is_some() {
            self.declaration_specifiers.push(DeclarationSpecifier::TypeSpec(Box::from(TypeSpecifier::Int)));
        } else if ctx.Char().is_some() {
            self.declaration_specifiers.push(DeclarationSpecifier::TypeSpec(Box::from(TypeSpecifier::Char)));
        }
    }

    fn visit_typeQualifier(&mut self, ctx: &TypeQualifierContext<'node>) {
        if ctx.Volatile().is_some() {
            self.declaration_specifiers.push(DeclarationSpecifier::TypeQual(Box::from(TypeQualifier::Volatile)));
        } else if ctx.Const().is_some() {
            self.declaration_specifiers.push(DeclarationSpecifier::TypeQual(Box::from(TypeQualifier::Const)));
        }
    }
}


pub fn generate_parse_tree(input: &str) {
    let lexer = olgaLexer::new(InputStream::new(input.into()));
    let source = CommonTokenStream::new(lexer);
    let mut parser = olgaParser::new(source);

    let parse_tree_root = parser.compilationUnit().expect("Parse tree root node");

    let mut parse_tree_visitor = Visitor::new();
    parse_tree_root.accept(&mut parse_tree_visitor);

    //let ast_root = parse_tree_visitor._nodes.pop();
    //ast_root
}
