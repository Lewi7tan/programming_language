#![allow(dead_code, unused_variables)]

use std::fmt;

pub struct GlobalSymbolTable {
    pub global_symbols: Vec<SymbolEntry>,
    pub scope_symbol_tables: Vec<ScopeSymbolTable>,
}

impl GlobalSymbolTable {
    pub fn new() -> Self {
        GlobalSymbolTable { global_symbols: vec![], scope_symbol_tables: vec![] }
    }

    pub fn insert_globally(&mut self, symbol_entry: SymbolEntry) {
        if self.is_declared_globally(&symbol_entry.id_name) {
            panic!("[!] Line: {} '{}' already declared.", symbol_entry.line_number, symbol_entry.id_name);
        }

        if symbol_entry.owns_scope() {
            self.scope_symbol_tables.push(ScopeSymbolTable::new(&symbol_entry.id_name));
        }

        self.global_symbols.push(symbol_entry);
    }

    pub fn insert_in_scope(&mut self, symbol_entry: SymbolEntry, scope_name: &str) {
        let scope = self.scope_symbol_tables.iter_mut()
            .find(|scope_symbol_table| &*scope_symbol_table.scope_name == scope_name);

        match scope {
            Some(scope_symbol_table) => scope_symbol_table.insert(symbol_entry),
            None => panic!("[!] Line: {} Cannot insert in scope '{}'. Scope not found!", symbol_entry.line_number, scope_name),
        }
    }

    pub fn lookup(&self, id_name: &str, scope_type: ScopeType) -> Option<&SymbolEntry> {
        let symbol_entry = match scope_type {
            ScopeType::Global => self.lookup_global_scope(id_name),
            ScopeType::Local(scope_name) => self.lookup_local_scope(id_name, &scope_name),
        };

        symbol_entry
    }

    pub fn print(&self) {
        println!("|    SCOPE     : LINE |    SYMBOL    |  DATATYPE   | TYPE");

        for symbol_entry in self.global_symbols.iter() {
            let data_type = symbol_entry.data_type.to_string();
            let symbol_type = symbol_entry.symbol_type.to_string();
            println!("|    GLOBAL    : {:^5}|{:^14}|{:^13}| {:^9}", symbol_entry.line_number, symbol_entry.id_name, data_type, symbol_type);

            if symbol_entry.owns_scope() {
                self.print_local_scope(symbol_entry);
            }
        }
    }

    fn print_local_scope(&self, symbol_entry: &SymbolEntry) {
        let scope = self.scope_symbol_tables.iter()
            .find(|scope_symbol_table| &*scope_symbol_table.scope_name == symbol_entry.id_name);

        match scope {
            None => println!("No Scope To Print"),
            Some(scope) => scope.print(),
        }
    }


    fn is_declared_globally(&self, id_name: &str) -> bool {
        if self.lookup_global_scope(id_name).is_some() {
            return true;
        }

        false
    }

    fn lookup_global_scope(&self, id_name: &str) -> Option<&SymbolEntry> {
        self.global_symbols.iter().find(|symbol_entry| &*symbol_entry.id_name == id_name)
    }

    fn lookup_local_scope(&self, id_name: &str, scope_name: &str) -> Option<&SymbolEntry> {
        let local_scope = self.scope_symbol_tables.iter()
            .find(|scope_symbol_table| &*scope_symbol_table.scope_name == scope_name);

        let symbol_entry = match local_scope {
            Some(local_scope) => local_scope.lookup(id_name),
            None => self.lookup_global_scope(id_name),
        };

        symbol_entry
    }
}

pub struct ScopeSymbolTable {
    pub scope_name: String,
    pub scope_symbols: Vec<SymbolEntry>,
}

impl ScopeSymbolTable {
    pub fn new(scope_name: &str) -> Self {
        ScopeSymbolTable { scope_symbols: vec![], scope_name: scope_name.to_string() }
    }

    pub fn insert(&mut self, symbol_entry: SymbolEntry) {
        if self.is_declared_in_scope(&symbol_entry.id_name) {
            panic!("[!] Line: {} '{}' already declared.", symbol_entry.line_number, symbol_entry.id_name);
        }

        self.scope_symbols.push(symbol_entry);
    }

    pub fn lookup(&self, id_name: &str) -> Option<&SymbolEntry> {
        self.scope_symbols.iter().find(|symbol_entry| &*symbol_entry.id_name == id_name)
    }

    pub fn print(&self) {
        for symbol_entry in &self.scope_symbols {
            let data_type = symbol_entry.data_type.to_string();
            let symbol_type = symbol_entry.symbol_type.to_string();
            println!("|{:^14}: {:^5}|{:^14}|{:^13}| {:^9}", self.scope_name, symbol_entry.line_number, symbol_entry.id_name, data_type, symbol_type);
        }
    }

    fn is_declared_in_scope(&self, id_name: &str) -> bool {
        if self.lookup(id_name).is_some() {
            return true;
        }

        false
    }
}

#[derive(PartialEq, Debug, Clone)]
pub struct SymbolEntry {
    pub id_name: String,
    pub data_type: DataType,
    pub symbol_type: SymbolType,
    pub line_number: u32,
}

impl SymbolEntry {
    fn owns_scope(&self) -> bool {
        if self.data_type == DataType::UserDefined || self.symbol_type == SymbolType::Function {
            return true;
        }

        false
    }
}

#[derive(PartialEq, Debug, Clone)]
pub enum DataType {
    Void,
    Int,
    Char,
    Float,
    UserDefined,
}

impl fmt::Display for DataType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(PartialEq, Debug, Clone)]
pub enum SymbolType {
    Variable,
    Function,
}

impl fmt::Display for SymbolType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

pub enum ScopeType {
    Global,
    Local(String),
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_global_insert() {
        let mut global_symtab = GlobalSymbolTable::new();
        let entry = SymbolEntry {
            id_name: "foo".to_string(),
            data_type: DataType::Void,
            symbol_type: SymbolType::Variable,
            line_number: 0,
        };

        global_symtab.insert_globally(entry.clone());

        let inserted_entry = global_symtab.global_symbols.pop().unwrap();

        assert_eq!(inserted_entry, entry);
    }

    #[test]
    #[should_panic]
    fn test_global_repeated_occurrence() {
        let mut global_symtab = GlobalSymbolTable::new();
        let entry = SymbolEntry {
            id_name: "foo".to_string(),
            data_type: DataType::Void,
            symbol_type: SymbolType::Variable,
            line_number: 0,
        };

        global_symtab.insert_globally(entry.clone());
        global_symtab.insert_globally(entry.clone());
    }

    #[test]
    #[should_panic]
    fn test_scope_repeated_occurrence() {
        let mut scope_symtab = ScopeSymbolTable::new("foo");
        let scope_entry = SymbolEntry {
            id_name: "bar".to_string(),
            data_type: DataType::Int,
            symbol_type: SymbolType::Variable,
            line_number: 0,
        };

        scope_symtab.insert(scope_entry.clone());
        scope_symtab.insert(scope_entry.clone());
    }

    #[test]
    fn test_scope_insert() {
        let mut scope_symtab = ScopeSymbolTable::new("foo");
        let scope_entry = SymbolEntry {
            id_name: "bar".to_string(),
            data_type: DataType::Int,
            symbol_type: SymbolType::Variable,
            line_number: 0,
        };

        scope_symtab.insert(scope_entry.clone());

        assert_eq!(scope_entry, scope_symtab.scope_symbols.pop().unwrap());
    }

    #[test]
    #[should_panic]
    fn test_non_existing_scope_insert() {
        let mut global_symtab = GlobalSymbolTable::new();

        let scope_entry = SymbolEntry {
            id_name: "".to_string(),
            data_type: DataType::Void,
            symbol_type: SymbolType::Variable,
            line_number: 0,
        };

        global_symtab.insert_in_scope(scope_entry, "foo");
    }

    #[test]
    fn test_function_scope_creation() {
        let mut global_symtab = GlobalSymbolTable::new();

        let function = SymbolEntry {
            id_name: "foo".to_string(),
            data_type: DataType::Void,
            symbol_type: SymbolType::Function,
            line_number: 0,
        };
        global_symtab.insert_globally(function.clone());

        let inserted_function_scope = global_symtab.scope_symbol_tables.pop().unwrap();
        let inserted_function = global_symtab.global_symbols.pop().unwrap();

        assert_eq!(inserted_function_scope.scope_name, function.id_name);
        assert_eq!(inserted_function, function);
    }

    #[test]
    fn test_filling_function_scope() {
        let mut global_symtab = GlobalSymbolTable::new();

        let function = SymbolEntry {
            id_name: "foo".to_string(),
            data_type: DataType::Void,
            symbol_type: SymbolType::Function,
            line_number: 0,
        };
        let local_variable = SymbolEntry {
            id_name: "bar".to_string(),
            data_type: DataType::Int,
            symbol_type: SymbolType::Variable,
            line_number: 0,
        };

        global_symtab.insert_globally(function.clone());
        global_symtab.insert_in_scope(local_variable.clone(), &function.id_name);

        let local_variable_scope = &mut global_symtab.scope_symbol_tables.pop().unwrap();
        let local_variable_entry = local_variable_scope.scope_symbols.pop().unwrap();
        assert_eq!(local_variable, local_variable_entry);
    }

    #[test]
    fn test_user_defined_type_scope_creation() {
        let mut global_symtab = GlobalSymbolTable::new();

        let user_defined = SymbolEntry {
            id_name: "foo".to_string(),
            data_type: DataType::UserDefined,
            symbol_type: SymbolType::Variable,
            line_number: 0,
        };
        global_symtab.insert_globally(user_defined.clone());

        let inserted_struct_scope = global_symtab.scope_symbol_tables.pop().unwrap();
        let inserted_struct = global_symtab.global_symbols.pop().unwrap();

        assert_eq!(inserted_struct_scope.scope_name, user_defined.id_name);
        assert_eq!(inserted_struct, user_defined);
    }


    #[test]
    fn test_global_symbol_table_lookup() {
        let mut global_symtab = GlobalSymbolTable::new();

        let symbol1 = SymbolEntry {
            id_name: "foo".to_string(),
            data_type: DataType::Void,
            symbol_type: SymbolType::Function,
            line_number: 1,
        };
        let symbol2 = SymbolEntry {
            id_name: "bar".to_string(),
            data_type: DataType::Int,
            symbol_type: SymbolType::Variable,
            line_number: 2,
        };
        global_symtab.insert_globally(symbol1.clone());
        global_symtab.insert_globally(symbol2.clone());

        let returned_symbol = global_symtab.lookup("foo", ScopeType::Global).unwrap();

        assert_eq!(returned_symbol, &symbol1);
    }

    #[test]
    fn test_scope_symbol_table_lookup() {
        let mut global_symtab = GlobalSymbolTable::new();

        let function = SymbolEntry {
            id_name: "foo".to_string(),
            data_type: DataType::Void,
            symbol_type: SymbolType::Function,
            line_number: 0,
        };
        let local_variable = SymbolEntry {
            id_name: "bar".to_string(),
            data_type: DataType::Int,
            symbol_type: SymbolType::Variable,
            line_number: 0,
        };

        global_symtab.insert_globally(function.clone());
        global_symtab.insert_in_scope(local_variable.clone(), "foo");

        let returned_symbol = global_symtab.lookup("bar",
                                                   ScopeType::Local("foo".to_string())).unwrap();

        assert_eq!(returned_symbol, &local_variable);
    }
}
